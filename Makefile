all: target/libmemb.a

target:
	mkdir -p $@

target/memb.o: memb.c | target
	gcc -c -o target/memb.o memb.c

target/libmemb.a: target/memb.o
	ar rcs target/libmemb.a target/memb.o

clean:
	rm -rf target
